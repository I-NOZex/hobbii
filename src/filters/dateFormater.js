import * as dayjs from 'dayjs'

export default function (val, format) {
    if (!val) return '';
    return dayjs(val).format(format);
}