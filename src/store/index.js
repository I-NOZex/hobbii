import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import project from './modules/project';
import timeEntry from './modules/time-entry';
import timeTracker from './modules/time-tracker';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState()],
    modules: { project, timeEntry, timeTracker },
});
