import Vue from 'vue';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';

dayjs.extend(duration)


const state = {
    projects: [
        {
            id: 1,
            name: 'Time Tracker',
            isBillable: true,
            billRate: 500,
            client: 'Hobbii',
            timeEntryIds: [],
            isActive: true,
            createdDate: '2021-03-07T13:12:50.000Z',
        },
        {
            id: 2,
            name: 'Research',
            isBillable: false,
            billRate: null,
            client: 'Google',
            timeEntryIds: [],
            isActive: true,
            createdDate: '2021-03-07T13:12:50.000Z',
        },        
        {
            id: 3,
            name: 'Student',
            isBillable: false,
            billRate: null,
            client: 'UCN',
            timeEntryIds: [],
            isActive: false,
            createdDate: '2021-03-07T13:12:50.000Z',
        },        
        {
            id: 4,
            name: 'Test',
            isBillable: false,
            billRate: null,
            client: 'Bob',
            timeEntryIds: [],
            isActive: true,
            createdDate: '2021-03-07T10:12:50.000Z',
        },             
    ],
};

const getters = {
    getProjectById : (state) => projectId => state.projects.find(p => p.id === projectId),

    getProjects : (state) => state.projects.sort((a, b) => {
        return new Date(b.createdDate) - new Date(a.createdDate);
    }),

    getActiveProjects : (state) => state.projects
        .filter(p => p.isActive)
        .sort((a, b) => {
            return new Date(b.createdDate) - new Date(a.createdDate);
        }),

    getProjectExtended : (state, getters) => {
        return state.projects.map(p => ({ ...p, hours: getters.getTotalProjectDuration(p.id), billed: getters.getTotalProjectBill(p.id)}));
    },

    countProjects : state => state.projects.length + 1,


    getTotalProjectDuration : (state, getters) => (projectId) => {
        let timeEntries = getters.getTimeEntriesForProject(projectId);
        let countTotalMilliseconds = 0;
        timeEntries.forEach(entry => {
            countTotalMilliseconds += dayjs(entry.endDateTime).diff(entry.startDateTime);
        })
        const duration = dayjs.duration(countTotalMilliseconds, 'milliseconds');

        return duration;
    },

    getTotalProjectBill : (state, getters) => (projectId) => {
        let timeEntries = getters.getTimeEntriesForProject(projectId);
        if(!timeEntries && timeEntries.length == 0) return;
        const totalMs = timeEntries
            .filter( t => 
                t.isBillable 
                && dayjs(t.startDateTime).isValid() 
                && dayjs(t.endDateTime).isValid()
            ) // entry is complete and billable
            .map( t => dayjs(t.endDateTime).diff(t.startDateTime) )
            .reduce( (total, currValue) => total += currValue, 0 );

        const totalHours = dayjs.duration(totalMs, 'milliseconds').asHours();
        const project = getters.getProjectById(projectId);
        const totalBill = project.billRate * totalHours;

        return totalBill;
    },    

    getTimeEntriesForProject : (getters, state, rootGetters, rootState) => (projectId) => {
        return rootState['timeEntry/getTimeEntriesByProjectId'](projectId);
    },
};

const actions = {

    assignTimeEntryToProject : ({commit} , {timeEntryId, projectId}) => {
        commit('SET_TIME_ENTRY', {projectId, timeEntryId})
    },

    removeProject : ({commit}, projectId) => {
        commit('REMOVE_PROJECT', projectId);
    },

    updateProject : ({commit, getters}, project) => {
        if(!project.id) {
            const newProjectId = getters.countProjects;
            project = {...project, ...{id: newProjectId, createdDate: new Date()}}
        }

        commit('UPDATE_PROJECT', {project, projectId: project.id});
    }

};

const mutations = {
    SET_TIME_ENTRY : (state, {projectId, timeEntryId}) => {
        state.projects
            .find(x => x.id == projectId).timeEntryIds
            .push(timeEntryId);
    },

    REMOVE_PROJECT : (state, {projectId}) => {
        const projectIndex = state.projects.findIndex(p => p.id == projectId)
        state.projects.splice(projectIndex, 1);
    },

    UPDATE_PROJECT : (state, {project, projectId}) => {
        const projectIndex = state.projects.findIndex(p => p.id === projectId);
        if(projectIndex >= 0) { //update
            Vue.set(state.projects, projectIndex, project)
        } else { //create
            state.projects.push(project)
        }
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
