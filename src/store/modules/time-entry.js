const state = {
    timeEntries: [
        {
            id: 1,
            startDateTime: '2021-03-25T15:00:00.000Z',
            endDateTime: '2021-03-25T17:23:00.000Z',
            taskDescription: 'Initial MVP',
            isBillable: true,
            tags: null,
            projectId: 1,
        },

        {
            id: 2,
            startDateTime: '2021-03-07T13:12:50.000Z',
            endDateTime: '2021-03-07T15:46:23.000Z',
            taskDescription: 'Final version',
            isBillable: true,
            tags: null,
            projectId: 1,
        },     
        
        {
            id: 3,
            startDateTime: '2021-02-21T14:12:50.000Z',
            endDateTime: '2021-02-21T15:01:23.000Z',
            taskDescription: 'Hobbii frontend developer',
            isBillable: true,
            tags: null,
            projectId: 2,
        }, 
        
        {
            id: 4,
            startDateTime: '2009-06-27T08:12:50.000Z',
            endDateTime: '2021-03-07T15:46:23.000Z',
            taskDescription: 'Bugfixing',
            isBillable: false,
            tags: null,
            projectId: null,
        },         
    ],
};

const getters = {
    countEntries : state => state.timeEntries.length + 1,
    getTimeEntriesByProjectId : (state) => projectId => {
        return state.timeEntries.filter(e => e.projectId === projectId)
    },
    getTimeEntries : state => state.timeEntries

};

const actions = {
    addTimeEntry: ({ commit, dispatch, getters }, timeEntry) => {
        return new Promise((resolve) => {
            //validate "timeEntry" parameter
            const projectId = timeEntry.projectId;
            const timeEntryId = getters.countEntries;
            timeEntry = {...timeEntry, ...{id: timeEntryId}}
            
            commit('ADD_TIME_ENTRY', {timeEntry}) 
            if(projectId) dispatch('project/assignTimeEntryToProject', {timeEntryId, projectId}, {root: true})
            
            resolve(timeEntry);
        });    
    },
    
    removeTimeEntry: ({ commit, state }, timeEntryId) => { 
        console.log(timeEntryId)
        const timeEntryIndex = state.timeEntries.findIndex(t => t.id === timeEntryId);
        if(timeEntryIndex < 0) return;
        commit('REMOVE_TIME_ENTRY', {timeEntryIndex});
    },
};

const mutations = {
    ADD_TIME_ENTRY: (state, {timeEntry}) => {
        state.timeEntries.push(timeEntry);
    },

    REMOVE_TIME_ENTRY: (state, {timeEntryIndex}) => {
        state.timeEntries.splice(timeEntryIndex, 1);
    },
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
