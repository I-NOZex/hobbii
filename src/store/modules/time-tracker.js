
const state = {
    timeTrackerModel: {
        projectId: null,
        startDateTime: "",
        endDateTime: null,
        taskDescription: null,
        tags: null,
        isBillable: null,
    },
    timerId: null,
};

const getters = {
    getTimeTracker : (state) => state.timeTrackerModel,
    isTimerRunning : state => !!state.timerId,
};

const actions = {
    setStartDateTime : ({commit}, startDateTime) => {
        commit('SET_START_DATETIME', startDateTime);
    },

    setEndDateTime : ({commit, dispatch}, endDateTime) => {
        dispatch('stopTimer');
        commit('SET_END_DATETIME', endDateTime);
    },
    
    startTimer : ({commit}, timerId) => {
        commit('SET_TIMER_ID', timerId);
    },

    stopTimer : ({commit, state}) => {
        clearInterval(state.timerId);
        commit('SET_TIMER_ID', null);
    },
    
    resetTimer : ({commit}) => {
        commit('RESET_TIMER');
    }
};

const mutations = {
    SET_START_DATETIME : (state, startDateTime) => {
        state.timeTrackerModel.startDateTime = startDateTime;
    },

    SET_END_DATETIME : (state, endDateTime) => {
        state.timeTrackerModel.endDateTime = endDateTime;
    },
    SET_TIMER_ID : (state, timerId) => {
        state.timerId = timerId
    },
    RESET_TIMER : (state) => {
        Object.assign(state, {
            timeTrackerModel: {
                projectId: null,
                startDateTime: "",
                endDateTime: null,
                taskDescription: null,
                tags: null,
                isBillable: null,
            },
            timerId: null,
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
};
