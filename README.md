# Hobbii - Bob's Time Tracker

Introduction
Bob works as a freelance developer and therefore he always works on many different projects for many different clients.
 
In order to better get an overview of the time he spent on each project he would like a tool where he can register new projects and stop start working on the projects.
 
He must also be able to see a list of the amount of hours he has spent on each project.
 
Changes must be persisted between stopping and starting the application.
 
Choice of frameworks and stack are completely optional but we like working with VueJS and Nuxt.
 
Upon completing the assignment, you should upload your code to a git repo and share the code with us.

Requirements
- Bob must not be able to create projects with the same name twice
- Bob must be able to remove projects
- Bob must be able to start and stop working on projects
- Bob must be able to see an overview of projects and the total time spent on a project in hours

Nice to have (Bonus tasks)
- Bob would like to invoice customers while working on projects, thus creating an invoice with his base price of 500 DKK per hour spent. Creating subsequent invoice for the same project should only contain the hours worked since the last invoice.
 
- Bob would like to mark projects as completed, thus removing them from the overview, they should however always be visible from a project archive view


